# Fun game inspired by Pokemon
# Copyright (c) 2019 Ben Goldberg
# Import libraries

# Battle file format:
#Initiator.OtherPlayer
#BuddyIndexInInitiatorsStorage.BuddyIndexInOthersStorage
#NameOfInitiatorsBuddy.NameOfOthersBuddy
#HPOfInitiatorsBuddy.HPOfOthersBuddy
#AtkDamageDoneToInit.AtkDamageDoneToOther
# If a parameter doesn't exist (first created) it would look like this:
#PENDING
#Initiator.OtherPlayer
#BuddyIndexInInitiatorsStorage
#NameOfInitiatorsBuddy
#HPOfInitiatorsBuddy
#AtkDamageDoneToInitWhichIsZeroAtStart.AtkDamageDoneToOther

import sys, pygame, random, time, threading
from os import walk, getcwd
# Initialize pygame
pygame.init()
pygame.font.init()

# The onslaught of global variables here:

# display/color stuff
size = width, height = 640, 480
green = 34, 139, 34
lt_green = 134, 239, 134
screen = pygame.display.set_mode(size)

# change to whatever font you want
font = pygame.font.Font("resources/Comfortaa.ttf", 18)
prompt = font.render("Type username then press enter.", False, (0, 0, 0), green)

pygame.display.set_caption("Buddy Game")
pygame.display.set_icon(pygame.image.load("resources/gameIcon.png"))
appeared = pygame.image.load("resources/appeared.png")
#special = False

# Creature dex

creatureFiles = [pygame.image.load("resources/BirdiePie.png"),
                 pygame.image.load("resources/BearBow.png"),
                 pygame.image.load("resources/HappyFace.png")]
creatureNames = ["Birdie Pie", "Bear-Bow", "Happy Face"]
creaturesSpecial = [True, False, False]

# Images

yellowberry = pygame.image.load("resources/yellowberry.png")
redberry = pygame.image.load("resources/redberry.png")
blueberry = pygame.image.load("resources/blueberry.png")
h = pygame.image.load("resources/help_v1.png")
helpIcon = pygame.image.load("resources/helpIcon.png")
g = pygame.image.load("resources/battle.png")
win = pygame.image.load("resources/win.png")
lose = pygame.image.load("resources/loss.png")


# I'm forgetful sometimes so you can type loss OR lose :)
loss = lose

# For randomness rates

specialRate = 24
regularRate = 12

yellow_multiplier = (1/4)
red_multiplier = (1/3)
blue_multiplier = (1/2)

# Attack/HP

specialHP = 200
regularHP = 150
specialA = 75
regularA = 50

# berry quantity

yellowberries = 5
redberries = 10
blueberries = 15

# Current creature data

creatureFile = None
creatureName = None
creatureLocation = [None, None]
creatureSpecial = None
creatureA = None
creatureHP = None

# avatar

avatarLocation = [0, 0]
avatar = pygame.image.load("resources/"+random.choice(["Yellow", "Green", "Blue", "Red"])+"ShirtAvatar.png")
available = False
found = False

user = ""

buddiesPath = ""
buddiesFile = None

prompting = True

# async sleep
def sleep(seconds):
    tStart = time.time()
    while (time.time() - tStart) < seconds:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                buddiesFile.close()
                exit()

# function to draw text easier

def drawText(text, x, y, foreground, background):
    global screen, font
    screen.blit(font.render(text, False, foreground, background), (x, y))
    pygame.display.flip()

# get username (your buddies' information is stored in a file for your username

while prompting:
    screen.fill(green)
    screen.blit(prompt, (0, 0))
    drawText(user, 0, 460, (255, 255, 255), green)
    pygame.display.flip()
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                prompting = False
            elif event.key == pygame.K_BACKSPACE:
                user = user[:-1]
            else:
                user += event.unicode

# Greet user

screen.fill(green)
drawText("Hi, "+user+"! Welcome!", 0, 0, (0, 0, 0), green)
sleep(0.75)

# Open file to store your buddies' info (closed when you close game)

buddiesPath = user+".buddies"
buddiesFile = open(buddiesPath, "+a")

# Spawn a buddy

def spawn():
    global creatureName, creatureFile, creatureLocation, creatureA, creatureHP, creatureSpecial
    creatureFile = random.choice(creatureFiles)
    creatureLocation = random.randint(80, 560), random.randint(80, 400)
    try:
        creatureName = creatureNames[creatureFiles.index(creatureFile)]
        creatureSpecial = creaturesSpecial[creatureFiles.index(creatureFile)]
        if (creatureSpecial):
            creatureA = random.randint(0, specialA)
            creatureHP = random.randint(0, specialHP)
        else:
            creatureA = random.randint(0, regularA)
            creatureHP = random.randint(0, regularHP)
    except Exception as e:
        creatureName = "default -- error"
        print(e)

# Move avatar location by arrow keys, this takes care of that

def listen():
    global avatar
    keys=pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        avatarLocation[0] -= 1
    if keys[pygame.K_RIGHT]:
        avatarLocation[0] += 1
    if keys[pygame.K_UP]:
        avatarLocation[1] -= 1
    if keys[pygame.K_DOWN]:
        avatarLocation[1] += 1
    if keys[pygame.K_r]:
        avatar = pygame.image.load("resources/RedShirtAvatar.png")
    if keys[pygame.K_g]:
        avatar = pygame.image.load("resources/GreenShirtAvatar.png")
    if keys[pygame.K_b]:
        avatar = pygame.image.load("resources/BlueShirtAvatar.png")
    if keys[pygame.K_y]:
        avatar = pygame.image.load("resources/YellowShirtAvatar.png")

# Read buddies' file

def readFromStorage(num):
    global buddiesFile
    buddiesFile.seek(0)
    buddies = buddiesFile.readlines()
    try:
        b = buddies[num].split(",")
        return b
    except:
        pass

# Battle code -- PvP

def battle():
    global avatarLocation, screen
    # If avatar close enough
    if (abs(avatarLocation[0] - 304) <= 50 and abs(avatarLocation[1] - 224) <= 50):
        #Fill green
        screen.fill(green)
        # prompt for user
        prompting = True
        opponent = ""
        b = font.render("Who do you want to battle?", False, (255, 255, 255), green)
        while prompting:
            screen.fill(green)
            screen.blit(b, (0, 0))
            drawText(opponent, 0, 460, (255, 255, 255), green)
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        prompting = False
                    elif event.key == pygame.K_BACKSPACE:
                        opponent = opponent[:-1]
                    else:
                        opponent += event.unicode
        # Set file vars to None
        battleFile = None
        battleFileContents = ["nope", "nope", "nope", "nope", "nope", "nope"]
        # Check if exists
        for q,t,yay in walk(getcwd()+"/battles"):
            pass
        for i in yay:
            if (user in i and opponent in i and i.endswith(".battle") and len(i) == len(user+opponent+"-.battle")):
                battleFile = open("battles/"+i, "r+")
                battleFile.seek(0)
                print(battleFile.read())
                battleFile.seek(0)
        if (battleFile != None):
            # Yes, read to contents
            battleFileContents = battleFile.readlines()
            battleFile.seek(0)
        # You're battling against ...
        print(battleFileContents)
        try:
            fname = battleFile.name
            battleFile.close()
            # Switch into write mode -- we've done all our reading
            battleFile = open(fname, "w+")
        except:
            pass
        screen.fill(green)
        drawText("You're battling against "+opponent+"!", 0, 0, (255, 255, 255), green)
        sleep(2)
        initiator = None
        # Initiator is 2 for pending -- different file structure
        if (battleFileContents[0] == "PENDING\n"):
            initiator = 2
        # If you haven't chosen...
        if (battleFileContents[0] == "PENDING\n" or battleFile == None):
            drawText("Select Buddy!", 0, 460, (255, 255, 255), green)
            # THEN CHOOSE!
            picked = False
            count = 0
            while not picked:
                drawText(str(readFromStorage(count)[0])+" -- "+str(readFromStorage(count)[1])+"HP, "+str(readFromStorage(count)[2])+" Attack                                                          ", 0, 0, (255, 255, 255), green)
                for event in pygame.event.get():
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_RETURN:
                            picked = True
                        elif event.key == pygame.K_DOWN:
                            count += 1
                            if (readFromStorage(count) == None):
                                count -= 1
                        elif event.key == pygame.K_UP:
                            count -= 1
                            if (readFromStorage(count) == None):
                                count += 1
                        else:
                            opponent += event.unicode
        else:
            # If chosen, set initiator var like I said before
            initiator = int(battleFileContents[0].replace("\n", "").split(".").index(user)) # 0 for initiator
            count = int(battleFileContents[1].split(".")[initiator].replace("\n", ""))
        # Load all data about the buddy
        name, hp, a = readFromStorage(count)
        hp = int(hp)
        a = int(a)
        # Have you lost? (you're notified when you knock out other person)
        if (initiator != 2 and initiator != None):
            # Set HP to what's recorded in the file.
            hp = int(battleFileContents[3].split(".")[initiator].replace("\n", ""))
            name = battleFileContents[2].split(".")[initiator].replace("\n", "")
            # if hp - atk damage is less than 0 then you lose
            if (hp - int(battleFileContents[4].split(".")[initiator].replace("\n", "")) < 0):
                screen.blit(lose, (0, 0))
                pygame.display.flip()
                sleep(2)
                return
        # you can still lose while pending...
        if (initiator == 2):
            if (hp - int(battleFileContents[5].split(".")[1].replace("\n", "")) < 0):
                screen.blit(lose, (0, 0))
                pygame.display.flip()
                sleep(2)
                return
        screen.fill(green)
        screen.blit(creatureFiles[creatureNames.index(name)], (0,0))
        drawText("Go, "+name+"!", 0, 460, (255, 255, 255), green)
        sleep(1)
        screen.fill(green)
        screen.blit(creatureFiles[creatureNames.index(name)], (0,0))
        drawText("Type 'a' to attack and 'd' to dodge, then press enter.", 0, 460, (255,255,255), green)
        picked = False
        move = -1
        while not picked:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        picked = True
                    if event.key == pygame.K_a:
                        move = 3
                    if event.key == pygame.K_d:
                        move = 30
                    if event.key == pygame.K_w and (pygame.key.get_mods() & pygame.KMOD_CAPS) != 0:
                        qwerty = True
                        while qwerty:
                            sleep(0.5)
                            if (random.randint(0, 50) == 1):
                                qwerty = False
                            screen.fill((random.randint(0,255), random.randint(0,255), random.randint(0,255)))
                            pygame.display.flip()
                            for event in pygame.event.get():
                                if event.type == pygame.QUIT:
                                    battleFile.close()
                                    buddiesFile.close()
                                    exit()
        
        if (move != 30):
            if (initiator != None and initiator != 2):
                # BAM! damage inflicted cause u didn't dodge
                screen.fill(green)
                drawText(opponent+"\'s"+" "+battleFileContents[2].split(".")[not initiator].replace("\n", "")+" did "+battleFileContents[4].split(".")[initiator].replace("\n", "")+" damage to your "+name+".", 0, 0, (255,255,255), green)
                hp -= int(battleFileContents[4].split(".")[initiator].replace("\n", ""))
                sleep(2)
                screen.fill(green)
                drawText("You now have "+str(hp)+" HP.", 0, 0, (255, 255, 255), green)
                valueToPut = battleFileContents[3].split(".")
                valueToPut[initiator] = str(hp)
                battleFileContents[3] = valueToPut[0]+"."+valueToPut[1]
                sleep(2)
            if (initiator == 2):
                # BAM! damage inflicted cause u didn't dodge
                screen.fill(green)
                drawText(opponent+"\'s"+" "+battleFileContents[3].replace("\n", "")+" did "+battleFileContents[5].split(".")[1].replace("\n", "")+" damage to your "+name+".", 0, 0, (255,255,255), green)
                hp -= int(battleFileContents[5].split(".")[1].replace("\n", ""))
                sleep(2)
                screen.fill(green)
                drawText("You now have "+str(hp)+" HP.", 0, 0, (255, 255, 255), green)
                valueToPut = battleFileContents[4]
                valueToPut = valueToPut.replace("\n", "")
                valueToPut += "."+str(hp)+"\n"
                print(valueToPut)
                battleFileContents[4] = valueToPut
                valueToPut = battleFileContents[2].replace("\n", "")
                valueToPut += "."+str(count)+"\n"
                battleFileContents[2] = valueToPut
                valueToPut = battleFileContents[3].replace("\n", "")
                valueToPut += "."+str(name)+"\n"
                battleFileContents[3] = valueToPut
                battleFileContents.pop(0) # Removes 'PENDING'
                sleep(2)
            screen.fill(lt_green)
            drawText(name+" did "+str(a)+" damage!", 0, 0, (255,255,255), lt_green)
            sleep(2)
            if (initiator != None):
                battleFileContents[4] = str(a)+".0"+"\n"
                battleFile.writelines(battleFileContents)
                battleFile.close()
                screen.fill(green)
                drawText("Saved!", 0, 0, (255,255,255), green)
            if (initiator == None):
                # Set up file, hasn't been created yet
                battleFile = open("battles/"+user+"-"+opponent+".battle", "w")
                battleFile.write("PENDING\n")
                battleFile.write(user+"."+opponent+"\n")
                battleFile.write(str(count)+"\n")
                battleFile.write(name+"\n")
                battleFile.write(str(hp)+"\n")
                battleFile.write("0."+str(a)+"\n")
                battleFile.close()
                screen.fill(green)
                drawText("You attacked "+opponent+"'s buddy!", 0, 0, (255,255,255), green)
                sleep(2)
        else:
            ## Wait after you say you dodged
            if (initiator != 2 and initiator != None):
                # you dodged! not pending
                screen.fill(green)
                drawText("You dodged "+battleFileContents[2].split(".")[initiator].replace("\n", "")+"'s attack!", 0, 0, (255,255,255), green)
                battleFileContents[4] = "0.0\n"
                battleFile.writelines(battleFileContents)
                battleFile.close()
                sleep(1)
            if (initiator == 2):
                # You dodged! Pending
                screen.fill(green)
                drawText("You dodged "+battleFileContents[3].replace("\n", "")+"'s attack!", 0, 0, (255,255,255), green)
                battleFileContents.pop(0)
                battleFileContents[4] = "0.0\n"
                battleFile.writelines(battleFileContents)
                battleFile.close()
                sleep(1)
            if (initiator == None):
                # Set up file, hasn't been created yet
                screen.fill(green)
                drawText("Dodged! Creating file...", 0, 0, (255,255,255), green)
                battleFile = open("battles/"+user+"-"+opponent+".battle", "w")
                battleFile.write("PENDING\n")
                battleFile.write(user+"."+opponent+"\n")
                battleFile.write(str(count)+"\n")
                battleFile.write(name+"\n")
                battleFile.write(str(hp)+"\n")
                battleFile.write("0.0\n")
                battleFile.close()
                screen.fill(green)
                drawText("Saved!", 0, 0, (255,255,255), green)
                sleep(1)
        
# Check -- in range of anything?

def check():
    global found, creatureLocation, avatarLocation, available
    if (available == True and (abs(avatarLocation[0] - creatureLocation[0]) < 100 and abs(avatarLocation[1] - creatureLocation[1]) < 100)):
        found = True
        screen.blit(creatureFile, creatureLocation)
    elif (found == True):
        screen.blit(creatureFile, creatureLocation)

    mouse = pygame.mouse

    if (mouse.get_pressed()[0] == 1):
        x, y = mouse.get_pos()
        if (x >= 304 and x <= 336 and y >= 224 and y <= 256):
            battle()
        if (x >= 608 and y <= 32):
            screen.fill(green)
            screen.blit(h, (0, 0))
            pygame.display.flip()
            done = False
            while not done:
                for event in pygame.event.get():
                    if event.type == pygame.KEYDOWN:
                        done = True

    if (mouse.get_pressed()[0] == 1 and available == True and found == True):
        x, y = mouse.get_pos()
        if (x >= creatureLocation[0] and x <= creatureLocation[0]+80 and y >= creatureLocation[1] and y <= creatureLocation[1]+80):
            encounter()

# Encounter code ... looooong

def encounter():
    global yellowberries, blueberries, redberries, creatureFile, available, found
    # green -> light green -> white with buddy in top left
    sleep(0.5)
    screen.fill(lt_green)
    pygame.display.flip()
    sleep(0.5)
    screen.fill((255, 255, 255))
    screen.blit(creatureFile, (0, 0))
    # Show berries
    if (yellowberries > 0):
        screen.blit(yellowberry, (204, 224))
    if (redberries > 0):
        screen.blit(redberry, (304, 224))
    if (blueberries > 0):
        screen.blit(blueberry, (404, 224))
    # Show berry amounts
    berry_amount = font.render(str(yellowberries)+" Yellow Berries, "+str(redberries)+" Red Berries, and "+str(blueberries)+" Blue Berries. Choose wisely!", False, (0, 0, 0), (255, 255, 255))
    screen.blit(berry_amount, (0, 460))
    pygame.display.flip()
    sleep(2)
    while True:
        mouse = pygame.mouse
        # Was game closed?
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                buddiesFile.close()
                exit()
        if (mouse.get_pressed()[0] == 1):
            # clicked!
            x, y = mouse.get_pos()
            # if the y is in berry range
            if (y >= 220 and y <= 256):
                # yellow berry?
                if (x >= 200 and x <= 225 and yellowberries > 0):
                    yellowberries -= 1
                    screen.fill((255, 255, 255))
                    screen.blit(creatureFile, (0, 0))
                    screen.blit(yellowberry, (0, 0))
                    pygame.display.flip()
                    # is it special? special -> reduced rate, higher possible A + HP
                    if creatureSpecial:
                        if (yellowberries > 0 and random.randint(1, (specialRate*yellow_multiplier)) == 1):
                            # RNG says it's your buddy
                            screen.blit(font.render(creatureName+" is now your buddy!", False, (0,0,0), (255,255,255)), (0, 460))
                            pygame.display.flip()
                            sleep(2)
                            # Store that!
                            buddiesFile.write(str(creatureName)+","+str(creatureHP)+","+str(creatureA)+'\n')
                            available = False
                            found = False
                            break
                        else:
                            # Sorry :( not your buddy
                            break
                    else:
                        # not special -- higher buddy rate
                        if (yellowberries > 0 and random.randint(1, (regularRate*yellow_multiplier)) == 1):
                            screen.blit(font.render(creatureName+" is now your buddy!", False, (0,0,0), (255,255,255)), (0, 460))
                            pygame.display.flip()
                            sleep(2)
                            buddiesFile.write(str(creatureName)+","+str(creatureHP)+","+str(creatureA)+'\n')
                            available = False
                            found = False
                            break
                        else:
                            break
                # red berry?
                elif (x >= 300 and x <= 325 and redberries > 0):
                    redberries -= 1
                    screen.fill((255, 255, 255))
                    screen.blit(creatureFile, (0, 0))
                    screen.blit(redberry, (0, 0))
                    pygame.display.flip()
                    if creatureSpecial:
                        if (redberries > 0 and random.randint(1, (specialRate*red_multiplier)) == 1):
                            screen.blit(font.render(creatureName+" is now your buddy!", False, (0,0,0), (255,255,255)), (0, 460))
                            pygame.display.flip()
                            sleep(2)
                            buddiesFile.write(str(creatureName)+","+str(creatureHP)+","+str(creatureA)+'\n')
                            available = False
                            found = False
                            break
                        else:
                            break
                            
                    else:
                        if (redberries > 0 and random.randint(1, (regularRate*red_multiplier)) == 1):
                            screen.blit(font.render(creatureName+" is now your buddy!", False, (0,0,0), (255,255,255)), (0, 460))
                            pygame.display.flip()
                            sleep(2)
                            buddiesFile.write(str(creatureName)+","+str(creatureHP)+","+str(creatureA)+'\n')
                            available = False
                            found = False
                            break
                        else:
                            break
                # blue berry?
                if (x >= 400 and x <= 425 and blueberries > 0):
                    blueberries -= 1
                    screen.fill((255, 255, 255))
                    screen.blit(creatureFile, (0, 0))
                    screen.blit(blueberry, (0, 0))
                    pygame.display.flip()
                    if creatureSpecial:
                        if (blueberries > 0 and random.randint(1, (specialRate*blue_multiplier)) == 1):
                            screen.blit(font.render(creatureName+" is now your buddy!", False, (0,0,0), (255,255,255)), (0, 460))
                            pygame.display.flip()
                            sleep(2)
                            buddiesFile.write(str(creatureName)+","+str(creatureHP)+","+str(creatureA)+'\n')
                            available = False
                            found = False
                            break
                        else:
                            break
                    else:
                        if (blueberries > 0 and random.randint(1, (regularRate*blue_multiplier)) == 1):
                            screen.blit(font.render(creatureName+" is now your buddy!", False, (0,0,0), (255,255,255)), (0, 460))
                            pygame.display.flip()
                            sleep(2)
                            buddiesFile.write(str(creatureName)+","+str(creatureHP)+","+str(creatureA)+'\n')
                            available = False
                            found = False
                            break
                        else:
                            break

while True:
    # there is a 1/1000 chance something will spawn if something isn't there.
    # not too slim given that the main loop runs every 1/45 second
    # so it should take around 22 seconds at maximum
    if (available == False):
        if (random.randint(0, 1000) == 18):
            spawn()
            available = True
            screen.fill(green)
            screen.blit(appeared, [320-16, 240-16])
            pygame.display.flip()
            sleep(1)
    # was the game closed?
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            buddiesFile.close()
            exit()
    # move avatar
    listen()
    screen.fill(green)
    screen.blit(g, (304, 224)) # draw gym icon
    screen.blit(helpIcon, (608, 0))
    # check -- in range of a buddy?
    check()
    # draw avatar
    screen.blit(avatar, avatarLocation)
    # update display
    pygame.display.flip()
    # wait a teensy bit
    sleep(1/45)
